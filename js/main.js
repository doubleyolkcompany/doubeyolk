var tag = document.createElement('script');
tag.src = "https://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
// Replace the 'ytplayer' element with an <iframe> and
// YouTube player after the API code downloads.
var player;
function onYouTubePlayerAPIReady() 
{
	player = new YT.Player('ytplayer', {
	height: '220',
	width: '400',
	videoId: 'ZaF-CjKlUOc'
	});
}

$(".east-west-circles-mobile").on("click", function() {
	var tl = new TimelineMax();
	//Check to see if the big circle is open
	if ($(".east-west-big-circle-mobile").css('visibility') === 'visible')
	{
		TweenMax.to(".east-west-big-circle-mobile", 1, {visibility:"hidden", autoAlpha:0});
		tl.to("#circle_west_mobile", 2, {
			scaleX:1,
			scaleY:1,
			transformOrigin:"50% 50%",
			ease: Power3.easeInOut
		}).call(function()
		{
			TweenMax.to("#circle_east_mobile", 2, {
				x:6,
				ease: Power3.easeInOut
			});
			TweenMax.to("#circle_west_mobile", 2, {
				x:-6,
				ease: Power3.easeInOut
			});
			TweenMax.to('.east-west-labels-mobile', 1, {delay:0.8, autoAlpha: 1});
			TweenMax.to('.west-label-mobile', 1, {x: -2, delay: 0.8, ease: Power1.easeInOut});
			TweenMax.to('.east-label-mobile', 1, {x: 0, delay: 0.8, ease: Power1.easeInOut});
		});
		return;
	}
	TweenMax.to('.east-west-labels-mobile', 1, {delay:0.4, autoAlpha: 0});
	TweenMax.to('.west-label-mobile', 1, {x: 10, delay: 0.4, ease: Power1.easeInOut});
	TweenMax.to('.east-label-mobile', 1, {x: -10, delay: 0.4, ease: Power1.easeInOut});
	tl.to("#circle_west_mobile", 2, {
		x:200,
		ease: Power3.easeInOut
	}).to("#circle_west_mobile", 2, {
		scaleX:2.4,
		scaleY:2.4,
		transformOrigin:"50% 50%",
		ease: Power3.easeInOut
	});
	TweenMax.to("#circle_east_mobile", 2, {
		x:-200,
		ease: Power3.easeInOut
	});
	if (e.currentTarget.id == 'circle_west')
	{
		$(".east-west-big-circle-title-mobile").html($('#design_west_content').data('title'));
		$(".east-west-big-circle-content-mobile").html($('#design_west_content').data('content'));
	}
	else
	{
		$(".east-west-big-circle-title-mobile").html($('#design_east_content').data('title'));
		$(".east-west-big-circle-content-mobile").html($('#design_east_content').data('content'));
	}
	TweenMax.to(".east-west-big-circle-mobile", 1, {delay: 3, visibility:"initial", autoAlpha:1});
	realignLabels();
});
$(".east-west-circles").on("click", function(e) {
	var tl = new TimelineMax();
	//Check to see if the big circle is open
	if ($(".east-west-big-circle").css('visibility') === 'visible')
	{
		TweenMax.to(".east-west-big-circle", 1, {visibility:"hidden", autoAlpha:0});
		tl.to("#circle_west", 2, {
			scaleX:1,
			scaleY:1,
			transformOrigin:"50% 50%",
			ease: Power3.easeInOut
		}).call(function()
		{
			TweenMax.to("#circle_east", 2, {
				x:40,
				ease: Power3.easeInOut
			});
			var tl2 = new TimelineMax();
			tl2.to("#circle_west", 2, {
				x:-40,
				ease: Power3.easeInOut
			}).call(function() {
				realignLabels();
				TweenMax.to('.east-west-labels', 1, {
					delay:0,
					autoAlpha: 1
				});
			});
		});
		return;
	}
	TweenMax.to('.east-west-labels', 1, {delay:0.4, autoAlpha: 0});
	TweenMax.to('.west-label', 1, {x: 150, delay: 0.4, ease: Power1.easeInOut});
	TweenMax.to('.east-label', 1, {x: -150, delay: 0.4, ease: Power1.easeInOut});
	tl.to("#circle_west", 2, {
		x:350,
		ease: Power3.easeInOut
	}).to("#circle_west", 2, {
		scaleX:2.4,
		scaleY:2.4,
		transformOrigin:"50% 50%",
		ease: Power3.easeInOut
	}).call(function() {
		realignLabels();
	if (e.currentTarget.id == 'circle_west')
	{
		$(".east-west-big-circle-title").html($('#design_west_content').data('title'));
		$(".east-west-big-circle-content").html($('#design_west_content').data('content'));
	}
	else
	{
		$(".east-west-big-circle-title").html($('#design_east_content').data('title'));
		$(".east-west-big-circle-content").html($('#design_east_content').data('content'));
	}
		TweenMax.to(".east-west-big-circle", 1, {delay: 0, visibility:"initial", autoAlpha:1});
	});
	TweenMax.to("#circle_east", 2, {
		x:-350,
		ease: Power3.easeInOut
	});
});
$('.scrollto-item').on('click', function(elem) {
	$('html, body').animate({
			scrollTop: $('.' + $(elem.currentTarget).data('target')).offset().top
		}, 1000);
});
function realignLabels()
{
	if (detectIE() === false)
	{
		var circleWestOffset = $('#circle_west').offset();
		$('.west-label').offset({left: circleWestOffset.left + 70});
		var circleEastOffset = $('#circle_east').offset();
		$('.east-label').offset({left: circleEastOffset.left + 70});
		$('.east-west-big-circle-title').offset({left: circleWestOffset.left});
		$('.east-west-big-circle-content').offset({left: circleWestOffset.left});
	}
}
$(window).resize(function(){
	if (detectSafari() === false)
		realignLabels();
});
$('.menu-open').on('click', function(e) {
	if (e.currentTarget.checked)
		$('.menu-item-label').fadeIn();
	else
		$('.menu-item-label').fadeOut();
});
$('.mobile-menu-open').on('click', function(e){
	if (e.currentTarget.checked)
		$('.mobile-menu-dialog').fadeIn();
	else
		$('.mobile-menu-dialog').fadeOut();
});
$('.mobile-menu-item').on('click', function(){
	$('.mobile-menu-dialog').fadeOut();
	$('#mobile-menu-open').prop('checked', false);
});
$('.btn-send-mail').on('click', function(){
	$.ajax( {
		url: "mailer.php?" + $('.form-mailer').serialize(),
		method: "GET"
	}).done(function() {
		alert( "Your contact information was sent. We will be in touch with you shortly" );
	}).fail(function() {
		alert( "We have experienced an error while trying to send your contact information. Please try again" );
	});
	$('.form-mailer')[0].reset();
});
function alternativeEastWestAnim()
{
	$('.section-four-yellow').html($('#safari_east_west_circles').html());
	var originalWestLabel = $('.safari-west-circle').html();
	var circleOpened = false;
	function animateEastWest(text)
	{
		$('.safari-west-circle').addClass('safari-west-circle-animation');
		$('.safari-east-circle').addClass('safari-east-circle-animation');
		setTimeout(function() {
			if (text === 'east')
				$('.safari-west-circle').html($('#design_east_content').html());
			else
				$('.safari-west-circle').html($('#design_west_content').html());
			$('.safari-east-circle').css('visibility','hidden');
			circleOpened = true;
		}, 2000);
	}
	function reverseEastWest()
	{
		$('.safari-west-circle').addClass('safari-west-circle-reverse-animation');
		$('.safari-east-circle').addClass('safari-east-circle-reverse-animation');
		$('.safari-west-circle').html('');
		setTimeout(function() {
			$('.safari-west-circle').html(originalWestLabel);
			$('.safari-east-circle').css('visibility','visible');
		}, 2000);
		setTimeout(function() {
			$('.safari-west-circle').removeClass('safari-west-circle-animation');
			$('.safari-east-circle').removeClass('safari-east-circle-animation');
			$('.safari-west-circle').removeClass('safari-west-circle-reverse-animation');
			$('.safari-east-circle').removeClass('safari-east-circle-reverse-animation');
			circleOpened = false;
		}, 4000);
	}
	$('.safari-west-circle').on('click', function(){
		if (circleOpened === true)
			reverseEastWest();
		else
			animateEastWest('west');
	});
	$('.safari-east-circle').on('click', function(){
		if (circleOpened === true)
			reverseEastWest();
		else
			animateEastWest('east');
	});
}
function detectIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }
    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }
    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // Edge (IE 12+) => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }
    // other browser
    return false;
}
function detectSafari()
{
	return /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
}

if (detectIE() !== false)
{
	// $('.animation-yolk-merge-unmerge').hide();
	// $('.landing-bg-container').css('background','40% no-repeat url(/img/bg_ie.gif)');
	// $('.landing-bg-container').css('padding-bottom', '30%');

	alternativeEastWestAnim();
}

if (detectSafari() === true)
{
	alternativeEastWestAnim();
}
else
	realignLabels();
