<?php

$name = filter_var($_GET['name'], FILTER_SANITIZE_STRING);
$fromEmail = filter_var($_GET['email'], FILTER_SANITIZE_STRING);
$contactNumber = filter_var($_GET['contact_number'], FILTER_SANITIZE_STRING);
$location = filter_var($_GET['location'], FILTER_SANITIZE_STRING);
$message = filter_var($_GET['message'], FILTER_SANITIZE_STRING);

// $to = get_option('admin_email');
$to = "jack@doubleyolk.co";

$subject = "Doubleyolk Contact Form";
$message = "User $name, with contact number $contactNumber and with location $location wrote the following message: \n\n".$message;

$headers = "From:" . $fromEmail;
mail($to,$subject,$message,$headers, '-f'.$fromEmail);